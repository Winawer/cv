# README

This is my CV, typeset in XeLaTeX based on a template from [Dario Taraborelli](http://nitens.org/taraborelli/cvtex).  Feel free to clone this repo and use it in any way that helps you. 